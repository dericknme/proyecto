import _ from '@lodash';
import clsx from 'clsx';

export const orderStatuses = [
  {
    id: 1,
    name: 'Inactivo',
    color: 'bg-red text-white',
  },
  {
    id: 2,
    name: 'Activo',
    color: 'bg-green-800 text-white',
  },
  {
    id: 3,
    name: 'InProcess',
    color: 'bg-purple-700 text-white',
  },
  {
    id: 4,
    name: 'Pendiente',
    color: 'bg-blue-700 text-white',
  },
  {
    id: 5,
    name: 'Pagado',
    color: 'bg-green-800 text-white',
  },
];

function OrdersStatus(props) {
  return (
    <div
      className={clsx(
        'inline text-12 font-semibold py-4 px-12 rounded-full truncate',
        _.find(orderStatuses, { name: props.name }).color
      )}
    >
      {props.name}
    </div>
  );
}

export default OrdersStatus;
