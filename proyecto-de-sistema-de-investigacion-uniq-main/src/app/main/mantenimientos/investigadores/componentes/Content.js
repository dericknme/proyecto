import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import axios from 'axios';
import RutaGeneral from '../../../../shared-components/rutaGeneral';
import ModalGrupoAcademico from './modalGrupoAcademico';
import PopperEliminar from './popperEliminar';

function GrupoAcademicoContent(props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const { datosInvestigadores, term } = props;
  const [estadoData, setEstadoData] = React.useState(false);

  React.useEffect(() => {
    axios
      .get(`${RutaGeneral}/mantenimientos/tipo_investigador/listar`)
      .then((res) => {
        props.setDatosInvestigadores(res.data);
        setEstadoData(true);
      })
      .catch((err) => {});
  }, [estadoData]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  function searchingTerm(value) {
    return function (x) {
      return (
        x.denominacion.toLowerCase().includes(value.toLowerCase()) ||
        x.id_tipo_investigador.toLowerCase().includes(value.toLowerCase()) ||
        !value
      );
    };
  }
 
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <div className="w-full flex flex-col">
      <FuseScrollbars className="flex-grow overflow-x-auto">
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center">ID</TableCell>
              <TableCell align="center">Denominacion</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {datosInvestigadores &&
              datosInvestigadores
                .filter(searchingTerm(term))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((value, index) => (
                  <TableRow key={index} hover tabIndex={-1}>
                    <TableCell align="center">{value.id_tipo_investigador}</TableCell>
                    <TableCell align="center">{value.denominacion}</TableCell>
                    <TableCell align="center">
                      <ModalGrupoAcademico
                        editar
                        data={value}
                        setDatosInvestigadores={props.setDatosInvestigadores}
                      />
                      <PopperEliminar value={value} />
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </FuseScrollbars>
      <TablePagination
        className="flex-shrink-0 border-t-1"
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={datosInvestigadores.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </div>
  );
}

export default GrupoAcademicoContent;
