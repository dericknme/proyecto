import { IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import Popper from '@mui/material/Popper';
import Typography from '@mui/material/Typography';
import Fade from '@mui/material/Fade';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import { useState } from 'react';
import axios from 'axios';
import RutaGeneral from 'app/shared-components/rutaGeneral';

const PopperEliminar = (props) => {
  const { value } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState();

  const handleClickPopper = (newPlacement) => (event) => {
    setAnchorEl(event.currentTarget);
    setOpen((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const eliminarEstado = (values) => {
    axios
      .delete(
        `${RutaGeneral}/mantenimientos/tipo_investigador/eliminar/${values.id_tipo_investigador}`
      )
      .then((res) => {
        axios
          .get(`${RutaGeneral}/mantenimientos/tipo_investigador/listar`)
          .then((res2) => {
            props.setDatosInvestigadores(res2.data);
          })
          .catch((err) => {});
      })
      .catch((err) => {});
  };
  return (
    <>
      <IconButton
        onClick={handleClickPopper('bottom-end')}
        color="secondary"
        aria-label="add to shopping cart"
      >
        <DeleteIcon />
      </IconButton>
      <Popper open={open} anchorEl={anchorEl} placement={placement} transition>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <Paper>
              <Typography className="p-10">
                Esta seguro de eliminar <b>{value.denominacion}</b> ?
              </Typography>
              <div className="grid grid-cols-2">
                <Button
                  className="m-5"
                  onClick={() => {
                    console.log(value);
                  }}
                  size="small"
                  variant="contained"
                  color="secondary"
                >
                  Eliminar
                </Button>
                <Button
                  className="m-5"
                  onClick={handleClose}
                  variant="contained"
                  color="secondary"
                  size="small"
                >
                  Cancelar
                </Button>
              </div>
            </Paper>
          </Fade>
        )}
      </Popper>
    </>
  );
};
export default PopperEliminar;
