import FusePageCarded from '@fuse/core/FusePageCarded';
import { styled } from '@mui/material/styles';
import { useState } from 'react';
import GrupoAcademicoHeader from './componentes/Header';
import GrupoAcademicoContent from './componentes/Content';

const Root = styled(FusePageCarded)(({ theme }) => ({
  '& .FusePageCarded-header': {
    minHeight: 72,
    height: 72,
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      minHeight: 136,
      height: 136,
    },
  },
  '& .FusePageCarded-content': {
    display: 'flex',
  },
  '& .FusePageCarded-contentCard': {
    overflow: 'hidden',
  },
}));
const Investigadores = () => {
  const [datosInvestigadores, setDatosInvestigadores] = useState([]);
  const [term, setTerm] = useState('');
  return (
    <Root
      header={
        <GrupoAcademicoHeader
        datosInvestigadores={datosInvestigadores}
        setDatosInvestigadores={setDatosInvestigadores}
          setTerm={setTerm}
        />
      }
      content={
        <GrupoAcademicoContent
        datosInvestigadores={datosInvestigadores}
        setDatosInvestigadores={setDatosInvestigadores}
          term={term}
        />
      }
      innerScroll
    />
  );
};
export default Investigadores;
