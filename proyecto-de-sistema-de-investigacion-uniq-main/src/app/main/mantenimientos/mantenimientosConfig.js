import Investigadores from './investigadores/index';
import PadronCursos from './padronCursos/index';
const MantenimientosConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: '/mantenimientos/investigadores',
      component: Investigadores,
    },
    {
      path: '/padroncursos',
      component: PadronCursos,
    }
  ],
};

export default MantenimientosConfig;
